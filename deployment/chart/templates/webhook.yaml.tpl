apiVersion: admissionregistration.k8s.io/v1
kind: ValidatingWebhookConfiguration
metadata:
  name: ingress-admission
  annotations:
    cert-manager.io/inject-ca-from: "{{ .Release.Namespace }}/{{ .Values.webhook.secretName }}"
webhooks:
  - name: ingress-admission.tools.wmcloud.org
    clientConfig:
      service:
        name: ingress-admission
        namespace: ingress-admission
        path: "/"
      caBundle: ""
    rules:
      - operations: ["CREATE","UPDATE"]
        apiGroups: ["networking.k8s.io"]
        apiVersions: ["v1"]
        resources: ["ingresses"]
    failurePolicy: "{{ .Values.webhook.failurePolicy }}"
    matchPolicy: Equivalent
    sideEffects: None
    admissionReviewVersions: ["v1"]
    namespaceSelector:
      matchExpressions:
        - key: name
          operator: NotIn
          values: {{ .Values.webhook.excludeNamespaces | toJson }}
