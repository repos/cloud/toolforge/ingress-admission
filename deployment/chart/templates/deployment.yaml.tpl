
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ingress-admission
  labels:
    name: ingress-admission
  annotations:
    secret.reloader.stakater.com/reload: "{{ .Values.webhook.secretName }}"
spec:
  replicas: {{ .Values.replicas }}
  selector:
    matchLabels:
      name: ingress-admission
  template:
    metadata:
      name: ingress-admission
      labels:
        name: ingress-admission
    spec:
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels:
              name: ingress-admission
      containers:
        - name: webhook
          image: "{{ .Values.image.name }}:{{ .Values.image.tag }}"
          imagePullPolicy: "{{ .Values.image.pullPolicy }}"
          env:
            - name: "DEBUG"
              value: "{{ .Values.debug }}"
            - name: "DOMAINS"
              value: "{{ .Values.domains | join "," }}"
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: webhook-certs
              mountPath: /etc/webhook/certs
              readOnly: true
          securityContext:
            readOnlyRootFilesystem: true
      volumes:
        - name: webhook-certs
          secret:
            secretName: "{{ .Values.webhook.secretName }}"
